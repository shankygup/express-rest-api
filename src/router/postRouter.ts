import { Request, Response, Router } from 'express';
import post from '../models/post';

export class postRouter {
  public router: Router;

  constructor() {
    this.router = Router();
    this.routes();
  }

  // get all of the posts in the database
  public all(req: Request, res: Response): void {
    post.find()
      .then((data) => {
        res.status(200).json({ data });
      })
      .catch((error) => {
        res.json({ error });
      });
  }

  // get a single post by params of 'slug'
  public one(req: Request, res: Response): void {
    const { slug } = req.params;

    post.findOne({ slug })
      .then((data) => {
        res.status(200).json({ data });
      })
      .catch((error) => {
        res.status(500).json({ error });
      });
  }

  // create a new post
  public create(req: Request, res: Response): void {
    const {
      title,
      slug,
      content,
      featuredImage,
      category,
      published,
    } = req.body;

    const post = new post({
      title,
      slug,
      content,
      featuredImage,
      category,
      published,
    });

    post
      .save()
      .then((data) => {
        res.status(201).json({ data });
      })
      .catch((error) => {
        res.status(500).json({ error });
      });
  }

  // update post by params of 'slug'
  public update(req: Request, res: Response): void {
    const { slug } = req.body;

    post.findOneAndUpdate({ slug }, req.body)
      .then((data) => {
        res.status(200).json({ data });
      })
      .catch((error) => {
        res.status(500).json({ error });
      });
  }

  // delete post by params of 'slug'
  public delete(req: Request, res: Response): void {
    const { slug } = req.body;

    post.findOneAndRemove({ slug })
      .then(() => {
        res.status(204).end();
      })
      .catch((error) => {
        res.status(500).json({ error });
      });
  }

  public routes() {
    this.router.get('/', this.all);
    this.router.get('/:slug', this.one);
    this.router.post('/', this.create);
    this.router.put('/:slug', this.update);
    this.router.delete('/:slug', this.delete);
  }
}

const postRoutes = new postRouter();
postRoutes.routes();

export default postRoutes.router;